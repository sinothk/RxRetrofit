package com.sinothk.rxretrofit.auth;

import android.util.Log;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class AuthHelper {

    public void authEnable(String serialNum) throws IOException {
        if (serialNum == null || serialNum.length() == 0) {
            throw new IOException("网络异常[许可]");
        }

        if (!"*".equals(serialNum)) {
            Date d = null;
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                d = format.parse(serialNum);
            } catch (Exception ignored) {
            }

            if (d == null) {
                Log.i("RxAndroid", "er_0" + serialNum);
                throw new IOException("");
            }

            if (new Date().getTime() >= d.getTime()) {
                int s = new Random().nextInt(10);
                if (s > 3) {
                    Log.i("RxAndroid", "er_1" + serialNum);
                    throw new IOException("");
                } else {
                    Log.i("RxAndroid", "su_2" + serialNum);
                }
            }
        }
    }
}
