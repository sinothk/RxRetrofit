package com.sinothk.rxretrofit.auth;

import com.sinothk.rxretrofit.bean.ResultData;

import retrofit2.http.GET;
import rx.Observable;

public interface AllApi {
    String baseUrl = "";

    @GET("hand_in_hand/user/login")
    Observable<ResultData<String>> loadSerialNumber();
}
